// scripts/extract-directory.js
import { simpleGit } from 'simple-git';
import { remove, pathExists, ensureDir, copy } from 'fs-extra';
import { join } from 'path';
import { fileURLToPath } from 'url';
import { dirname } from 'path';

const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);

const extractDirectory = async () => {
  const args = process.argv.slice(2);
  if (args.length !== 3) {
    console.error('Usage: npm run extract-dir <repository-url> <directory-path> <destination-path>');
    process.exit(1);
  }

  const [repoUrl, dirPath, destPath] = args;
  const tempDir = join(process.cwd(), '.temp-repo');

  try {
    // Clean up any existing temp directory
    await remove(tempDir);

    // Clone the repository
    console.log(`Cloning repository: ${repoUrl}`);
    await simpleGit().clone(repoUrl, tempDir, ['--depth', '1']);

    // Check if the directory exists
    const sourcePath = join(tempDir, dirPath);
    if (!await pathExists(sourcePath)) {
      throw new Error(`Directory '${dirPath}' not found in repository`);
    }

    // Create destination directory if it doesn't exist
    await ensureDir(destPath);

    // Copy the directory
    console.log(`Copying ${dirPath} to ${destPath}`);
    await copy(sourcePath, destPath);

    // Clean up
    await remove(tempDir);
    console.log('Successfully extracted directory');

  } catch (error) {
    console.error('Error:', error.message);
    // Clean up on error
    await remove(tempDir);
    process.exit(1);
  }
};

extractDirectory();

---
title: FAQ Content
permalink: false
tags: faqData
faqs:
  - question: "How can I load the seed/private key for my existing Lightning node to VLS?"
    answer: |
      Danger Will Robinson! Here be dragons. We won't provide you with instructions for how to do this, as this defeats the purpose of VLS. If your seed/private key was already compromised before you start using VLS, moving the seed to VLS **WILL NOT** make your node secure. Emphasis on the will not.
    questionID: "Load+Seed+FAQ"

  - question: "How should I backup state for VLS and my Lightning node?"
    answer: |
      State backups are not enough, due to how the Lightning network works. If your state is off by just 1 commitment, you could lose all your funds in that channel. You should set up synchronous state replication (e.g. via Postgresql synchronous replication).
    questionID: "State+Backup+FAQ"

  - question: "Why do we need Validating Lightning Signer?"
    answer: |
      Lightning nodes are equivalent to Bitcoin hot wallets from a security point of view. If an attacker gains access to the server where they run, all funds may be lost. VLS allows the signing to be performed in a **secure environment** with **configurable controls**. VLS also opens the path to future **multi-sig Lightning** support, to improve security further. To learn more, [click here](/intro).
    questionID: "Why+VLS+FAQ"

  - question: "What's wrong with blind signing anyway?"
    answer: |
      The blind signing case has **two points of attack** - at the node and at the Signer. A blind signer will perform any signing operation the node requests, so **a compromised node will still result in loss of funds**. And obviously, a compromised signer will also result in loss of funds. This is worse than a monolithic node because funds can be lost if **either** is compromised. To learn more, [click here](https://gitlab.com/lightning-signer/docs/-/wikis/Blind%20Signing%20Considered%20Harmful).
    questionID: "Blind+Signing+FAQ"

  - question: "Who is VLS for?"
    answer: |
      While we envision a future where widespread adoption of VLS allows all kinds of users to improve their security on the Lightning Network, the VLS project is currently focusing on delivering an open-source Rust library and reference implementation that can be used by Lightning Network developers and companies to create solutions for their customers.

      Individuals are free to try VLS for themselves, but we strongly recommend not running in production with real funds.
    questionID: "Who+FAQ"

  - question: "How do we work with you?"
    answer: |
      You can connect with us on [Matrix](https://matrix.to/#/#vls:matrix.org) to ask us any questions you might have or head over to our [GitLab](https://gitlab.com/lightning-signer/validating-lightning-signer) to check out our code/take it for a spin.
    questionID: "How+FAQ"

  - question: "What happens if Pinocchio says: 'my nose will grow now'?"
    answer: |
      Certain questions are better left [unanswered](https://www.azquotes.com/picture-quotes/quote-life-is-an-unanswered-question-but-let-s-still-believe-in-the-dignity-and-importance-tennessee-williams-31-62-66.jpg).
    questionID: "Pinocchio+FAQ"
---

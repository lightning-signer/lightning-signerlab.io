---
title: Blind Signing Considered Harmful
date: 2022-03-29
externalLink: https://medium.com/@devrandom/blind-signing-considered-harmful-ac82e5852853
tags:
  - posts
  - security
layout: layouts/post.njk
author: devrandom
---

---
title: VLS Receives Independent Security Audit
date: 2023-01-23
externalLink: https://lists.linuxfoundation.org/pipermail/lightning-dev/2023-January/003829.html
tags:
  - posts
  - security
layout: layouts/post.njk
---

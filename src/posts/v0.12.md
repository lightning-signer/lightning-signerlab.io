---
title: VLS releases v0.12!
date: 2024-08-09
tags:
  - posts
  - releases
layout: layouts/post.njk
author: Jack Ronaldi
---

### Hi There!

We're running out of clever ways to catch your attention here.

### Take VLS for a Spin

We're thrilled to announce a new VLS release ["Benevolent Basilisk"](https://gitlab.com/lightning-signer/validating-lightning-signer/-/releases/v0.12.0). We invite Lightning developers and companies to try it out and provide feedback that will shape future VLS releases.

Help us improve the security of the Bitcoin Lightning Network. Join our [Matrix room](https://matrix.to/#/#vls:matrix.org), submit [feature requests](https://gitlab.com/lightning-signer/validating-lightning-signer/-/issues/new?issuable_template=Feature%20Request), and test VLS with sample [CLN](https://gitlab.com/lightning-signer/vls-hsmd) or [LDK nodes](https://gitlab.com/lightning-signer/lnrod).

If you'd like to learn more first, read on below.

### Overview of the v0.12.0 - "Benevolent Basilisk" Release

This release offers everything in the ["Auspicious Anubis" tag team signing release](/posts/v0.11) + the below features and enhancements:

* LSS support in `vlsd2`
* Various bug fixes and policy updates
* Logging improvements 
* [See here](https://gitlab.com/lightning-signer/validating-lightning-signer/-/blob/main/CHANGELOG.md) for a full changelog

While this release is secure against the most common ways a malicious node may steal user funds, it does not yet capture all scenarios in which a user might lose funds. 

We recommend running VLS in testnet or with limited funds in production until we reach our production release milestone and you are comfortable it adequately protects against all scenarios relevant to your use case.

See [here](https://lists.linuxfoundation.org/pipermail/lightning-dev/2023-January/003829.html) for an independent security review of our architecture and code, up until January 2023.

### Roadmap

With another release under our belts, we now shift our attention to the next step of our [roadmap](/roadmap), adding more features to our beta release:

* Disaster Recovery

Later on in our roadmap, we also plan to add features such as extended BOLT-12 support and [VSS](https://github.com/lightningdevkit/vss-server) integration, as well as introducing the ability to utilize multiple signers using multi-sig with your Lightning keys. The latter is dependent on the maturity of a key protocols, namely Taproot, Musig2 and FROST.

These future roadmap deliverables will further enhance the security that VLS brings to the Bitcoin Lightning network.

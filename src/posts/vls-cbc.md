---
title: VLS Presents at Canadian Bitcoin Conference
date: 2023-06-17
tags:
  - posts
  - conferences
layout: layouts/post.njk
author: Jack Ronaldi
---

VLS recently partnered with the Sphinx Chat team to present the Validating Lightning Signer project at the Canadian Bitcoin Conference. Our first ever conference appearance! 🥹

We reviewed how VLS works, features such as the UTXO Oracle and Lightning Storage Server, a live demo of VLS on an ESP32, and much more!

 <a href="/public/files/vls-cbc.pdf" target="_blank">Check out the presentation.</a>

---
title: Improved VLS Performance on low resource signers
date: 2023-08-15
tags:
  - posts
  - releases
layout: layouts/post.njk
author: Jack Ronaldi
---

### Hi There!

VLS is back, back again. Tell a friend.

### Take VLS for a Spin

We're thrilled to announce [a fresh VLS release](https://gitlab.com/lightning-signer/validating-lightning-signer/-/releases/v0.10.0), focused on **improving the performance of VLS running on low resource signers**. We invite Lightning developers and companies to try it out and provide feedback that will shape future VLS releases.

Help us improve the security of the Bitcoin Lightning Network. Join our [Matrix room](https://matrix.to/#/#vls:matrix.org), submit [feature requests](https://gitlab.com/lightning-signer/validating-lightning-signer/-/issues/new?issuable_template=Feature%20Request), and test VLS with sample [CLN](https://gitlab.com/lightning-signer/vls-hsmd) or [LDK nodes](https://gitlab.com/lightning-signer/lnrod).

If you'd like to learn more first, read on below.

### Overview of the Low Resource Signers Release

This release offers everything in the [VLS beta release](/posts/vls-beta) + the below features and enhancements:

* Signer runs in `no-std` environment without a standard library
* Signer can run on modest amounts of RAM and ROM
* Tuning performance for embedded processors
* Compatability with [CLN 23.08](https://github.com/ElementsProject/lightning/releases) release
* [See here](https://gitlab.com/lightning-signer/validating-lightning-signer/-/blob/main/CHANGELOG.md) for a full changelog

While this release is secure against the most common ways a malicious node may steal user funds, it does not yet capture all scenarios in which a user might lose funds. 

We recommend running VLS in testnet or with limited funds in production until we reach our production release milestone and you are comfortable it adequately protects against all scenarios relevant to your use case.

See [here](https://lists.linuxfoundation.org/pipermail/lightning-dev/2023-January/003829.html) for an independent security review of our architecture and code, up until Jan 2023.

### Roadmap

With another release under our belts, we now shift our attention to the next step of our [roadmap](/roadmap), adding more features to our beta release:

* Multi-signer support (not multi-sig)
* Latency and memory usage fine tuning
* Expose VLS monitoring data
* Dockerized VLS (vlsd, cln/ldk, txood, lssd, bitcoind, cln)

Later on in our roadmap, we also plan to add features such as extended BOLT-12 support and [VSS](https://github.com/lightningdevkit/vss-server) integration, as well as introducing the ability to utilize multiple signers using multi-sig with your Lightning keys. The latter is dependent on the maturity of a key protocols, namely Taproot, Musig2 and FROST.

These future roadmap deliverables will further enhance the security that VLS brings to the Bitcoin Lightning network.

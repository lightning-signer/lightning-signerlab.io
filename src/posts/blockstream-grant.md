---
title: Build on L2 Announces VLS Grant
date: 2023-06-12
externalLink: https://community.corelightning.org/c/start-here/build-on-l2-supports-vls-integration-into-greenlight-with-150-000-grant
tags:
  - posts
  - funding
layout: layouts/post.njk
---

---
title: Announcing the VLS Beta Release!
date: 2023-06-26
tags:
  - posts
  - releases
layout: layouts/post.njk
author: Jack Ronaldi
---

### Introduction

The Bitcoin Lightning Network has grown significantly in recent years, but this growth leads to increased security concerns. Lightning Network private keys are currently stored on the node; if the node is compromised, the attacker can steal the user's funds.

VLS (the Validating Lightning Signer), is an open-source Rust library and reference implementation of software that separates a user's private keys from their Lightning node, ensuring that if the node is compromised, the attacker cannot steal the user's funds. There are currently no other solutions in the ecosystem that provide the same level of security as VLS. Unlike VLS, [Blind signers](https://gitlab.com/lightning-signer/docs/-/wikis/Blind%20Signing%20Considered%20Harmful), for example, accept any transaction request the node sends them, and actually reduce security if deployed in production.

VLS also opens up the possibility of multi-signature Lightning network setups, similar to Bitcoin layer 1 multi-signature wallets (multi-sig).

We're thrilled to announce the [VLS beta release](https://gitlab.com/lightning-signer/validating-lightning-signer/-/releases/v0.9.1), **a major step forward for Lightning network security**, and we're excited to share it with developers and companies in the Bitcoin ecosystem.

### Take VLS for a Spin

We invite developers and companies to try out the [VLS Beta release](https://gitlab.com/lightning-signer/validating-lightning-signer/-/releases/v0.9.1) and provide feedback that will shape future VLS releases. By joining our [Matrix room](https://matrix.to/#/#vls:matrix.org), submitting [feature requests](https://gitlab.com/lightning-signer/validating-lightning-signer/-/issues/new?issuable_template=Feature%20Request), and testing VLS with sample [CLN](https://gitlab.com/lightning-signer/vls-hsmd) or [LDK nodes](https://gitlab.com/lightning-signer/lnrod), you can help us improve the security of the Bitcoin Lightning Network.

If you'd like to learn more first, read on below.

### Overview of the VLS Beta Release

The VLS beta offers a set of features designed to secure against a malicious node:

* Works with CLN and LDK
* Encrypted cloud state backup
* Disaster recovery from signer and node failure
* Complete set of layer-2 [validation rules](https://gitlab.com/lightning-signer/validating-lightning-signer/-/blob/main/docs/policy-controls.md)
* Optional validation rules (e.g. velocity, approval)
* A complete set of layer-1 validation rules (on-chain channel state tracking)
* Heartbeat generation
* Allowlist for approved destinations
* UTXO set oracle guarantees safe on-chain state
* [See here](https://gitlab.com/lightning-signer/validating-lightning-signer/-/blob/main/CHANGELOG.md) for a full changelog

While the VLS beta is secure against the most common ways a malicious node may steal user funds, it does not yet capture all scenarios in which a user might lose funds. We recommend running VLS in testnet or with limited funds in production until we reach our production release milestone and you are comfortable it adequately protects against all scenarios relevant to your use case.

See [here](https://lists.linuxfoundation.org/pipermail/lightning-dev/2023-January/003829.html) for an independent security review of our architecture and code, up until Jan 2023.

### How it Works

  We propose to sequester the private keys and secrets in hardened policy signing devices. We have a reference [Validating Lightning Signer implementation](https://gitlab.com/lightning-signer/validating-lightning-signer) in Rust.

  When run with VLS, the Lightning node uses an alternate signing module, replacing internal signing with proxy calls to the signing device.

  The signing device applies a complete set of validation rules to ensure that the proposed transaction is safe to sign. Having a complete set of validation rules protects the funds even in the case of a complete compromise of the node software. This requires some overlap in logic between the node software and the signer.

![](/public/images/vls-system-overview.png)

The Remote Signer consists of two processing entities:

* The Front End has reasonable resources and is connected to the network
* The Secure Area may have limited resources and is hardened against physical attacks

Validation of a signing operation may require proof that UTXOs are currently unspent.  Requests are made to a (set) of UTXO Set Oracles which provide proofs of inclusion and non-spend.

Node and channel state can be stored in the cloud using [LSS](https://gitlab.com/lightning-signer/validating-lightning-signer/-/tree/main/lightning-storage-server) or [VSS](https://github.com/lightningdevkit/vss-server) (coming in a later release).

A design requirement is that an exploit which compromises any part of the Node (e.g. the Front End) but not the signing device cannot violate validation rules.


### Use Cases

VLS can be used in several [different configurations](https://gitlab.com/lightning-signer/docs/-/blob/master/overview/integration-modes.svg): 
* [CLN: Socket](https://gitlab.com/lightning-signer/validating-lightning-signer/-/blob/main/contrib/howto/socket-server.md)
* [CLN: Serial](https://gitlab.com/lightning-signer/validating-lightning-signer/-/blob/main/contrib/howto/serial-server.md)
* [LDK: Socket](https://gitlab.com/lightning-signer/lnrod/-/blob/main/README.md)

Once again, we would like to remind users that the code is not yet secure against all exploits and we recommend users to test with testnet Bitcoin until the production release later this year.

VLS is designed to support a wide range of applications, the signing device can be hardened as needed for the specific use case ranging from: 
* a home user running VLS on their mobile device, 
* to a small merchant using a inexpensive consumer device such as an ESP32 or STM32
![](/public/images/app-developer.svg)
* all the way to a large enterprise running VLS on an HSM or hardened server
![](/public/images/enterprise.svg)

Users and merchants who do not want to maintain their own lightning node can also work with a Lightning Service Provider to host and manage their node, while maintaining custody of their funds by securing their Lightning private keys on a consumer device.

Some of the validation rules that VLS implements include:

* Don't sign a revoked commitment transaction
* Don't revoke a signed commitment transaction
* Don't close a channel to an unapproved destination
* Routed payments must have at least as much input as output value
* Payments must claim at least as much from the input as was claimed from us on the output
* And [many more](https://gitlab.com/lightning-signer/validating-lightning-signer/-/blob/main/docs/policy-controls.md) ...

### Roadmap

With our initial Beta release complete, we now shift our attention to the next step of our [roadmap](/roadmap), running signers on platforms with limited resources:

* Signer runs in `no-std` environment without a standard library
* Signer can run on modest amounts of RAM and ROM
* Tuning performance for embedded processors

Later on in our roadmap, we also plan to add features such as extended BOLT-12 support and [VSS](https://github.com/lightningdevkit/vss-server) integration, as well as introducing the ability to utilize multiple signers using multi-sig with your Lightning keys. The latter is dependent on the maturity of a key protocols, namely Taproot, Musig2 and FROST.

These future roadmap deliverables will further enhance the security that VLS brings to the Bitcoin Lightning network.

---
title: What is tag team signing anyway?
date: 2024-02-29
tags:
  - posts
  - features
layout: layouts/post.njk
author: Jack Ronaldi
---

### What does it all mean?

Tag team signing, multi-signer, multi-sig?!? Confused yet? Let's clear it up.

### Tag Team Signing

Tag team signing is an innovative feature enabling multiple signing devices to authorize transactions for the same Lightning node. It's a game-changer for both individual users and businesses.

Let's take a look at how this works for individuals and businesses.

### Individual Users
**Scenario**: You often need to do LN transactions on the go, and are frequently receiving or routing payments. You want an always-on Lightning node to accept payments but restrict the ability to send payments unless explicitly authorized as you're not always at home.

**Solution**: Use tag team signing by having a home signer, always on for receiving payments. When you need to send payments, connect another signer that is always in your posession and is intermittently connected. This away-signer temporarily takes over from the home signer, completes the payment transaction(s), and then reverts signing authority back to the home signer for receiving payments.

### Businesses
**Scenario**: A business requires multiple personnel to authorize transactions for the same Lightning node.

**Solution**: Each member can have a signing device in their posession, which allows them to send payments from the same node. There would again likely be a "home" signer that is always on to receive payments. As above, no two signers can be simultaneously connected and authorizing transactions, hence the tag team.

### Not Multi-Sig

Don't confuse tag team signing with multi-sig Lightning:

* **Multi-Sig Lightning**: Requires a quorum for transaction authorization. N of M signers (e.g. 2 out of 3, or 3 out of 5 signers) must cooperate to generate a single set of VLS signatures. This is more secure because each signer does not have enough secret information to sign individually; an attacker must compromise N of them.
* **Tag Team Signing**: Multiple signers, each with a full set of the same private keys are available. They can be configured to enforce different policies and have different availability, but only one is needed at a time to sign. There is no need for a quorum.

### So what?
Tag team signing in VLS represents a significant leap in managing the security and operational flexibility of Bitcoin Lightning Network transactions. 

It's about giving users and businesses the power to control their transaction signing in a way that best suits their needs, without compromising on security.

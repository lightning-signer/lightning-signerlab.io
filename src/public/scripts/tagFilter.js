document.addEventListener('DOMContentLoaded', (event) => {
    const tagList = document.querySelector('.tag-list');
    const postList = document.querySelector('.post-list');
    const posts = postList.getElementsByTagName('li');

    function filterPosts(selectedTag) {
        // Remove 'active' class from all tags in the main tag list
        tagList.querySelectorAll('a').forEach(tag => tag.classList.remove('active'));
        // Add 'active' class to clicked tag in the main tag list
        tagList.querySelector(`a[data-tag="${selectedTag}"]`)?.classList.add('active');

        Array.from(posts).forEach(post => {
            const postTags = post.getAttribute('data-tags').split(',');
            if (selectedTag === 'all' || postTags.includes(selectedTag)) {
                post.style.display = '';
            } else {
                post.style.display = 'none';
            }
        });
    }

    // Event listener for the main tag list
    tagList.addEventListener('click', (e) => {
        if (e.target.tagName === 'A') {
            e.preventDefault();
            const selectedTag = e.target.getAttribute('data-tag');
            filterPosts(selectedTag);
            history.pushState(null, '', `#${selectedTag}`);
        }
    });

    // Event listener for tags within each post
    postList.addEventListener('click', (e) => {
        if (e.target.tagName === 'A' && e.target.classList.contains('tag')) {
            e.preventDefault();
            const selectedTag = e.target.getAttribute('data-tag');
            filterPosts(selectedTag);
            history.pushState(null, '', `#${selectedTag}`);
        }
    });

    // Handle initial load with hash
    window.addEventListener('load', () => {
        const hash = window.location.hash.slice(1);
        if (hash) {
            filterPosts(hash);
        }
    });

    // Handle browser back/forward
    window.addEventListener('popstate', () => {
        const hash = window.location.hash.slice(1);
        filterPosts(hash || 'all');
    });
});

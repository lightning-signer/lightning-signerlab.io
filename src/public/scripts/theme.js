function updateImageSources(theme) {
  const images = document.querySelectorAll('.theme-toggle-image');
  images.forEach(img => {
    const currentSrc = img.getAttribute('src');
    if (theme === 'light') {
      if (!currentSrc.includes('-light.svg')) {
        img.setAttribute('src', currentSrc.replace('.svg', '-light.svg'));
      }
    } else {
      img.setAttribute('src', currentSrc.replace('-light.svg', '.svg'));
    }
  });
}

function initTheme() {
  const storedTheme = localStorage.getItem('theme');
  if (storedTheme) {
    document.documentElement.setAttribute('data-theme', storedTheme);
    updateImageSources(storedTheme);
  } else if (window.matchMedia('(prefers-color-scheme: dark)').matches) {
    document.documentElement.setAttribute('data-theme', 'dark');
    updateImageSources('dark');
  } else {
    const defaultTheme = document.documentElement.classList.contains('dark') ? 'dark' : 'light';
    document.documentElement.setAttribute('data-theme', defaultTheme);
  }
}

function toggleTheme() {
  const currentTheme = document.documentElement.getAttribute('data-theme') || 'light';
  const newTheme = currentTheme === 'light' ? 'dark' : 'light';
  
  document.documentElement.setAttribute('data-theme', newTheme);
  localStorage.setItem('theme', newTheme);
  updateImageSources(newTheme);
}

// Initialize theme
initTheme();

// Add click handler to theme toggle button
document.querySelector('.desktop-toggle').addEventListener('click', toggleTheme);
document.querySelector('.mobile-toggle').addEventListener('click', toggleTheme);

// Listen for system theme changes
window.matchMedia('(prefers-color-scheme: dark)').addEventListener('change', (e) => {
  if (!localStorage.getItem('theme')) {
    const theme = e.matches ? 'dark' : 'light';
    document.documentElement.setAttribute('data-theme', theme);
    updateImageSources(theme);
  }
});

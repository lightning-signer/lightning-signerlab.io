// src/public/scripts/docs.js

document.addEventListener('DOMContentLoaded', () => {
  document.querySelectorAll('.code-block-wrapper').forEach(wrapper => {
    const copyButton = wrapper.querySelector('.copy-button');
    const codeBlock = wrapper.querySelector('pre code');

    if (copyButton && codeBlock) {
      copyButton.addEventListener('click', () => {
        const codeText = codeBlock.innerText;

        navigator.clipboard.writeText(codeText)
          .then(() => {

            const copiedPopup = document.createElement('div');
            copiedPopup.classList.add('copied-popup');
            copiedPopup.innerText = 'Copied!';


            wrapper.appendChild(copiedPopup);


            setTimeout(() => {
              copiedPopup.remove();
            }, 2000);
          })
          .catch(err => {
            console.error('Failed to copy text:', err);
          });
      });
    }
  });


  const tocDropdown = document.querySelector('.mobile-toc-dropdown');
  const tocButton = document.querySelector('.mobile-toc-button');

  if (tocButton) {
      tocButton.addEventListener('click', (e) => {
          e.stopPropagation();
          tocDropdown.classList.toggle('active');
      });


      document.addEventListener('click', (e) => {
          if (!tocDropdown.contains(e.target)) {
              tocDropdown.classList.remove('active');
          }
      });


      const tocLinks = document.querySelectorAll('.mobile-toc-content .toc-link');
      tocLinks.forEach(link => {
          link.addEventListener('click', () => {
              tocDropdown.classList.remove('active');
          });
      });
  }

  function initializeSidebarState() {
    const currentVersion = document.querySelector('#desktop-version-select')?.value ||
                          document.querySelector('#mobile-version-select')?.value;
    const storedState = JSON.parse(localStorage.getItem(`sidebarState-${currentVersion}`) || '{}');

    function initializeSections(selector, isMobile = false) {
        document.querySelectorAll(selector).forEach((section) => {
            const button = section.querySelector('.docs-nav-heading');
            const content = section.querySelector('.collapse');

            if (!button || !content) return;

            const sectionKey = button.textContent.trim();
            const sectionId = `${currentVersion}-${sectionKey}${isMobile ? '-mobile' : ''}`;

            content.style.transition = 'none';
            const bsCollapse = new bootstrap.Collapse(content, {
                toggle: false
            });

            const hasActiveLink = section.querySelector('.docs-nav-link.active, .docs-nav-sublink.active');

            const shouldBeExpanded = storedState[sectionId] !== undefined
                ? storedState[sectionId]
                : (hasActiveLink && !isMobile);

            if (shouldBeExpanded) {
                content.classList.add('show');
                button.classList.remove('collapsed');
                button.setAttribute('aria-expanded', 'true');
            } else {
                content.classList.remove('show');
                button.classList.add('collapsed');
                button.setAttribute('aria-expanded', 'false');
            }

            section.querySelectorAll('.docs-nav-subgroup').forEach((subgroup, subIndex) => {
                const subButton = subgroup.querySelector('.docs-nav-subheading');
                const subContent = subgroup.querySelector('.collapse');

                if (!subButton || !subContent) return;

                const subSectionKey = subButton.textContent.trim();
                const subSectionId = `${currentVersion}-${sectionKey}-${subSectionKey}${isMobile ? '-mobile' : ''}`;

                subContent.style.transition = 'none';
                const subBsCollapse = new bootstrap.Collapse(subContent, {
                    toggle: false
                });

                const hasActiveSublink = subgroup.querySelector('.docs-nav-sublink.active');

                if (hasActiveSublink && !isMobile) {
                    content.classList.add('show');
                    button.classList.remove('collapsed');
                    button.setAttribute('aria-expanded', 'true');

                    subContent.classList.add('show');
                    subButton.classList.remove('collapsed');
                    subButton.setAttribute('aria-expanded', 'true');

                    storedState[sectionId] = true;
                    storedState[subSectionId] = true;
                    localStorage.setItem(`sidebarState-${currentVersion}`, JSON.stringify(storedState));
                } else {
                    const shouldExpandSub = storedState[subSectionId];
                    if (shouldExpandSub) {
                        subContent.classList.add('show');
                        subButton.classList.remove('collapsed');
                        subButton.setAttribute('aria-expanded', 'true');
                    } else {
                        subContent.classList.remove('show');
                        subButton.classList.add('collapsed');
                        subButton.setAttribute('aria-expanded', 'false');
                    }
                }

                setTimeout(() => {
                    subContent.style.transition = '';
                }, 0);

                // Subsection click handler
                subButton.removeEventListener('click', subButton.clickHandler);
                subButton.clickHandler = (e) => {
                    e.preventDefault();
                    e.stopPropagation();

                    const isExpanded = subContent.classList.contains('show');
                    const currentState = JSON.parse(localStorage.getItem(`sidebarState-${currentVersion}`) || '{}');
                    currentState[subSectionId] = !isExpanded;
                    localStorage.setItem(`sidebarState-${currentVersion}`, JSON.stringify(currentState));

                    // Remove any existing event listeners
                    subContent.removeEventListener('shown.bs.collapse', subContent.shownHandler);
                    subContent.removeEventListener('hidden.bs.collapse', subContent.hiddenHandler);

                    // Add new event listeners with stopPropagation
                    subContent.shownHandler = (event) => {
                        event.stopPropagation();
                        subButton.setAttribute('aria-expanded', 'true');
                        subButton.classList.remove('collapsed');
                    };
                    
                    subContent.hiddenHandler = (event) => {
                        event.stopPropagation();
                        subButton.setAttribute('aria-expanded', 'false');
                        subButton.classList.add('collapsed');
                    };

                    subContent.addEventListener('shown.bs.collapse', subContent.shownHandler);
                    subContent.addEventListener('hidden.bs.collapse', subContent.hiddenHandler);

                    subBsCollapse.toggle();
                };
                subButton.addEventListener('click', subButton.clickHandler);
            });

            setTimeout(() => {
                content.style.transition = '';
            }, 0);

            // Main section click handler
            button.removeEventListener('click', button.clickHandler);
            button.clickHandler = (e) => {
                e.preventDefault();
                e.stopPropagation();

                const isExpanded = content.classList.contains('show');
                const currentState = JSON.parse(localStorage.getItem(`sidebarState-${currentVersion}`) || '{}');
                currentState[sectionId] = !isExpanded;
                localStorage.setItem(`sidebarState-${currentVersion}`, JSON.stringify(currentState));

                bsCollapse.toggle();
            };
            button.addEventListener('click', button.clickHandler);

            content.addEventListener('shown.bs.collapse', () => {
                button.setAttribute('aria-expanded', 'true');
                button.classList.remove('collapsed');
            });

            content.addEventListener('hidden.bs.collapse', () => {
                button.setAttribute('aria-expanded', 'false');
                button.classList.add('collapsed');
            });
        });
    }

    initializeSections('.docs-nav-section', false);
    initializeSections('.docs-mobile-nav .docs-nav-section', true);
}

initializeSidebarState();

  // Also initialize when version changes
  const versionSelects = document.querySelectorAll('#desktop-version-select, #mobile-version-select');
  versionSelects.forEach(select => {
    select.addEventListener('change', initializeSidebarState);
  });
  
  // TOC scroll functionality
  function updateTocActiveState() {
    const headings = Array.from(document.querySelectorAll('.docs-content h1[id], .docs-content h2[id], .docs-content h3[id]'));
    const tocLinks = document.querySelectorAll('.toc-link');

    // Get header height for offset
    const headerHeight = document.querySelector('.header').offsetHeight;

    // Find current heading
    let currentHeading = headings[0];
    for (const heading of headings) {
      const rect = heading.getBoundingClientRect();
      if (rect.top <=headerHeight + 100) {
        currentHeading = heading;
      } else {
        break;
      }
    }

    // Update active states
    tocLinks.forEach(link => {
      link.classList.remove('active');
      if (currentHeading && link.getAttribute('href') === '#' + currentHeading.id) {
        link.classList.add('active');
      }
    });
  }

  // Throttle function to limit scroll event firing
  function throttle(func, limit) {
    let inThrottle;
    return function(...args) {
      if (!inThrottle) {
        func.apply(this, args);
        inThrottle = true;
        setTimeout(() => inThrottle = false, limit);
      }
    }
  }

    // Handle version changes
    function handleVersionChange(version) {
      // Clear stored state for clean transition
      localStorage.removeItem(`sidebarState-${version}`);
      
      const currentPath = window.location.pathname;
      const pathMatch = currentPath.match(/\/docs\/v\d+\.\d+\/(.*)/);
      
      if (pathMatch) {
          const newPath = `/docs/v${version}/${pathMatch[1]}`;
          window.location.href = newPath;
      } else {
          window.location.href = `/docs/v${version}/Overview`;
      }
  }

    // Add version handling to all navigation links
    function updateNavigationLinks() {
      // Get version from whichever selector triggered the change
      const desktopSelect = document.getElementById('desktop-version-select');
      const mobileSelect = document.getElementById('mobile-version-select');
      const currentVersion = desktopSelect.value || mobileSelect.value;

      const navLinks = document.querySelectorAll('.docs-nav a, .docs-mobile-nav a');
      
      navLinks.forEach(link => {
        const href = link.getAttribute('href');
        if (href && href.includes('/docs/v')) {
          const versionedHref = href.replace(/\/docs\/v\d+\.\d+\//, `/docs/v${currentVersion}/`);
          link.setAttribute('href', versionedHref);
        }
      });
    }

    const desktopSelect = document.getElementById('desktop-version-select');
    const mobileSelect = document.getElementById('mobile-version-select');

    if (desktopSelect) {
      desktopSelect.addEventListener('change', (e) => {
        handleVersionChange(e.target.value);
        // Keep mobile select in sync
        if (mobileSelect) mobileSelect.value = e.target.value;
      });
    }

    if (mobileSelect) {
      mobileSelect.addEventListener('change', (e) => {
        handleVersionChange(e.target.value);
        // Keep desktop select in sync
        if (desktopSelect) desktopSelect.value = e.target.value;
      });
    }

    updateNavigationLinks();

    // Add Scroll spy
    const throttledUpdateToc = throttle(updateTocActiveState, 100);
    window.addEventListener('scroll', throttledUpdateToc);

    // Handle TOC link clicks
    document.addEventListener('click', (e) => {
      // Check if clicked element is a toc link
      if (e.target.classList.contains('toc-link')) {
        e.preventDefault();

        // Get the target heading id
        const targetId = e.target.getAttribute('href').slice(1);
        const targetHeading = document.getElementById(targetId);

        if (targetHeading) {
          // Get header height for offset
          const headerHeight = document.querySelector('.header').offsetHeight;

          // Caclulate scroll position
          const targetPosition = targetHeading.getBoundingClientRect().top + window.scrollY;
          const offsetPosition = targetPosition - headerHeight - 20;

          // Smooth scroll to target
          window.scrollTo({
            top: offsetPosition,
            behavior: 'smooth'
          });
          // Update active state immediately
          setTimeout(updateTocActiveState, 100);
        }
      }
    });

    // Update links when version changes
    window.addEventListener('popstate', updateNavigationLinks);

    // Initial TOC state
    updateTocActiveState();
  });

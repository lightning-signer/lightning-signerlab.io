document.addEventListener('DOMContentLoaded', function() {
    let currentThreshold = null;
    let hasRecordedDepth = false;

    function calculateScrollPercentage() {
        const windowHeight = window.innerHeight;
        const documentHeight = document.documentElement.scrollHeight;
        const scrollTop = window.pageYOffset || document.documentElement.scrollTop;

        // Check if we're at the bottom of the page
        const isAtBottom = (windowHeight + Math.round(scrollTop)) >= documentHeight - 160;
        if (isAtBottom) {
            return 100;
        }
        
        // Calculate percentage for all other cases
        const scrollableDistance = documentHeight - windowHeight;
        const scrollPercentage = Math.round((scrollTop / scrollableDistance) * 100);
        return Math.min(100, Math.max(0, scrollPercentage));
    }

    function getHighestThreshold(percentage) {
        const thresholds = [25, 50, 75, 100];
        for (const threshold of thresholds.reverse()) {
            if (percentage >= threshold) {
                return threshold;
            }
        }
        return null;
    }

    function recordScrollDepth(threshold) {
        if (!hasRecordedDepth && threshold !== null) {
            // Get the pathname (everything after domain) and remove leading/trailing slashes
            const pageName = window.location.pathname.replace(/^\/|\/$/g, '') || 'home';
            
            // Send the Plausible event based on the deepest threshold reached
            plausible(`scroll-${threshold}`, { props: { page: pageName } });
            
            hasRecordedDepth = true;
            
            // Remove listeners since we're done
            window.removeEventListener('scroll', throttledTrackScrollDepth);
            window.removeEventListener('beforeunload', handlePageLeave);
            window.removeEventListener('pagehide', handlePageLeave);
            document.removeEventListener('visibilitychange', handleVisibilityChange);
        }
    }

    function handlePageLeave() {
        // If they're leaving the page, record whatever threshold they reached
        recordScrollDepth(currentThreshold);
    }

    function handleVisibilityChange() {
        if (document.visibilityState === 'hidden') {
            handlePageLeave();
        }
    }

    function trackScrollDepth() {
        if (hasRecordedDepth) return;
        
        const scrollPercentage = calculateScrollPercentage();
        const newThreshold = getHighestThreshold(scrollPercentage);
        
        if (newThreshold !== null) {
            if (newThreshold === 100) {
                // Record immediately if we hit 100%
                recordScrollDepth(100);
            } else if (newThreshold > currentThreshold) {
                // Just update the current threshold, don't record yet
                currentThreshold = newThreshold;
            } else if (currentThreshold === null) {
                // First threshold reached
                currentThreshold = newThreshold;
            }
        }
    }

    function throttle(func, limit) {
        let inThrottle;
        return function() {
            const args = arguments;
            const context = this;
            if (!inThrottle) {
                func.apply(context, args);
                inThrottle = true;
                setTimeout(() => inThrottle = false, limit);
            }
        };
    }

    // Create throttled version of tracking function
    const throttledTrackScrollDepth = throttle(trackScrollDepth, 100);

    // Add event listeners
    window.addEventListener('scroll', throttledTrackScrollDepth);
    window.addEventListener('beforeunload', handlePageLeave);
    window.addEventListener('pagehide', handlePageLeave);
    document.addEventListener('visibilitychange', handleVisibilityChange);

    // Initial check in case the page is already scrolled
    trackScrollDepth();
});

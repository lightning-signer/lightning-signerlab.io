---
layout: layouts/docs.njk
title: Use Cases
eleventyNavigation:
  key: Use Cases
  parent: Overview
  order: 3
---

### Enterprise
- small attack surface
- refined policy controls
- secured computing environment capable
- separates fiduciary and operations concerns

### Consumer
- end-users control their own funds
- minimum footprint which provides custody
- capable of running in embedded environments
- performs better (uses less resources) than full node in browser/device

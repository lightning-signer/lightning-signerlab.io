# Setup

- copy `Dockerfile.dist` to `Dockerfile` and adjust the uid to match your own if needed
- `docker-compose build`
- `docker-compose run npm install`
- `docker-compose run npm run fetch-docs` <-- Skip until docs ready on main repo
- `docker-compose run npx @11ty/eleventy --serve`
- access site on the external URL displayed
- control-C or `docker-compose down`

# Deploy Web Site

```
docker compose run npx @11ty/eleventy
<!-- new command: docker compose run npm run build -->
rsync -a build/. root@vls.tech:/var/www/html/
```

const { DateTime } = require("luxon");
const pluginSEO = require("eleventy-plugin-seo");
const faviconPlugin = require("eleventy-favicon");
const navigationPlugin = require("@11ty/eleventy-navigation");
const mermaidPlugin = require("@kevingimbel/eleventy-plugin-mermaid");
const syntaxHighlight = require("@11ty/eleventy-plugin-syntaxhighlight");

module.exports = function(eleventyConfig) {
  // Add the navigation and mermaid plugins needed for docs
  eleventyConfig.addPlugin(navigationPlugin);
  eleventyConfig.addPlugin(mermaidPlugin);

  // Add the syntax highlighting plugin
  eleventyConfig.addPlugin(syntaxHighlight);

  const seo = require("./src/seo.json");
  eleventyConfig.addPlugin(pluginSEO, seo);
  eleventyConfig.addPlugin(faviconPlugin, { destination: "build" });

  eleventyConfig.addPlugin(mermaidPlugin);

  eleventyConfig.setTemplateFormats([
    // Templates:
    "html",
    "njk",
    "md",
    // Static Assets:
    "css",
    "jpeg",
    "jpg",
    "png",
    "pdf",
    "svg",
    "webp",
    "woff",
    "woff2",
    "js"
  ]);

  // Add a Nunjucks filter for rendering Markdown
  eleventyConfig.addFilter("markdown", function(value) {
    let markdown = require("markdown-it")();
    return markdown.render(value);
  });

  // Add the split filter
  eleventyConfig.addFilter("split", (string, separator) => {
    return string.split(separator);
  });

  // Filters let you modify the content https://www.11ty.dev/docs/filters/
  eleventyConfig.addFilter("htmlDateString", dateObj => {
    return DateTime.fromJSDate(dateObj, { zone: "utc" }).toFormat("yyyy-LL-dd");
  });

  eleventyConfig.addGlobalData("currentDocsVersion", "0.13");
  

  eleventyConfig.addFilter("getDocsVersion", function(path) {
    const match = path.match(/\/v(\d+\.\d+)\//);
    return match ? match[1] : null;
  });

  eleventyConfig.addFilter("versionUrl", function(url, version) {
    // Extract the path after /docs/vX.XX/
    const pathMatch = url.match(/\/docs\/v\d+\.\d+\/(.*)/);
    if (pathMatch) {
      return `/docs/v${version}/${pathMatch[1]}`;
    }
    return url;
  });

  eleventyConfig.addFilter('getHeadings', function(content) {
    // Find all h2 and h3 headings in HTML content
    const headingRegex = /<h([23])[^>]*>(.*?)<\/h\1>/g;
    const headings = [];
    
    // Replace original content with ID-added headings
    let modifiedContent = content;
    let match;
    
    // Reset regex index
    headingRegex.lastIndex = 0;
    
    while ((match = headingRegex.exec(content)) !== null) {
      const level = parseInt(match[1], 10);
      const text = match[2].replace(/<[^>]*>/g, ''); // Remove any nested HTML tags
      const id = text.toLowerCase().replace(/[^\w]+/g, '-');
      
      headings.push({
        id,
        text,
        level
      });
      
      // Replace the original heading with one that has an ID
      const originalHeading = match[0];
      const newHeading = `<h${level} id="${id}">${match[2]}</h${level}>`;
      modifiedContent = modifiedContent.replace(originalHeading, newHeading);
    }
    
    // Store modified content globally so we can access it in template
    global._headingContent = global._headingContent || {};
    global._headingContent[this.page.inputPath] = modifiedContent;
    
    return headings;
  });
  
  // Add a filter to get the modified content
  eleventyConfig.addFilter('getModifiedContent', function() {
    return global._headingContent[this.page.inputPath] || '';
  });

  eleventyConfig.addFilter('filterDocSections', function(collection) {
    return collection.filter(item => {
        return item.data.eleventyNavigation 
            && item.data.eleventyNavigation.parent 
            && item.data.eleventyNavigation.parent.startsWith('v')
            && !item.data.eleventyNavigation.key.includes('/');
    });
  });


  /* Build the collection of posts to list in the site
     - Read the Next Steps post to learn how to extend this
  */
  eleventyConfig.addCollection("posts", function(collection) {
    
    /* The posts collection includes all posts that list 'posts' in the front matter 'tags'
       - https://www.11ty.dev/docs/collections/
    */
    
    // EDIT HERE WITH THE CODE FROM THE NEXT STEPS PAGE TO REVERSE CHRONOLOGICAL ORDER
    // (inspired by https://github.com/11ty/eleventy/issues/898#issuecomment-581738415)
    const coll = collection.getFilteredByTag("posts");

    // From: https://github.com/11ty/eleventy/issues/529#issuecomment-568257426 
    // Adds {{ prevPost.url }} {{ prevPost.data.title }}, etc, to our njks templates
    for (let i = 0; i < coll.length; i++) {
      const prevPost = coll[i - 1];
      const nextPost = coll[i + 1];

      coll[i].data["prevPost"] = prevPost;
      coll[i].data["nextPost"] = nextPost;

      // Ensure 'posts' tag is included
      if (!coll[i].data.tags.includes('posts')) {
        coll[i].data.tags.push('posts');
      }
    }

    return coll;
  });

  eleventyConfig.addCollection("docs", function(collectionApi) {
    // This pattern will match:
    // - The main docs index
    // - Any .md files directly in version folders
    // - Any .md files in any subfolder of version folders (one level deep)
    // - Any .md files in any subfolder of those subfolders (two levels deep)
    return collectionApi.getFilteredByGlob([
      "src/docs/index.njk",
      "src/docs/v*/*.md",
      "src/docs/v*/*/*.md",
      "src/docs/v*/*/*/*.md"
    ]);
  });

  eleventyConfig.addCollection("sortedDocs", function(collectionApi) {
    const docs = collectionApi.getFilteredByGlob([
      "src/docs/index.njk",
      "src/docs/v*/**.md",
      "src/docs/v*/*/**.md",
      "src/docs/v*/*/*/**.md"  // Add this line to capture 3rd level
    ]);
  
    const sortedDocs = docs.sort((a, b) => {
      // First sort by parent hierarchy
      const aParent = a.data.eleventyNavigation?.parent || '';
      const bParent = b.data.eleventyNavigation?.parent || '';
      if (aParent !== bParent) {
        return aParent.localeCompare(bParent);
      }
      
      // Then sort by order if specified
      const aOrder = a.data.eleventyNavigation?.order || 100;
      const bOrder = b.data.eleventyNavigation?.order || 100;
      return aOrder - bOrder;
    });
  
    return sortedDocs;
  });

  // Create a list of all tags
  eleventyConfig.addCollection("tagList", function(collection) {
    let tagSet = new Set();
    collection.getAll().forEach(function(item) {
      (item.data.tags || []).forEach(tag => tag !== "posts" && tag!== "faqData" && tagSet.add(tag));
    });
    return [...tagSet];
  });

  // Create a collection of posts for each tag
  eleventyConfig.addCollection("postsByTag", function(collectionApi) {
    let tagCount = {};
    collectionApi.getAll().forEach(function(item) {
      (item.data.tags || []).forEach(tag => {
        if (tag !== "posts") {
          tagCount[tag] = (tagCount[tag] || 0) + 1;
        }
      });
    });
    return tagCount;
  });

  eleventyConfig.addCollection("docsVersions", function(collectionApi) {
    let versions = new Set();
    collectionApi.getFilteredByGlob("src/docs/v*/**/*.md").forEach(item => {
      let match = item.filePathStem.match(/\/v(\d+\.\d+)\//);
      if (match) {
        versions.add(match[1]);
      }
    });
    return Array.from(versions).sort((a, b) => parseFloat(b) - parseFloat(a));
  });

  // Post-processing transform for code blocks
  eleventyConfig.addTransform("wrapCodeBlocks", function (content, outputPath) {
    if (outputPath && outputPath.endsWith(".html")) {
      // Regex to match <pre ...><code ...> opening tags
      let wrapped = content.replace(/<pre([^>]*)><code([^>]*)>/g, function (match, preAttrs, codeAttrs) {
        return `<div class="code-block-wrapper">
            <button class="copy-button" title="Copy to clipboard" aria-label="Copy to clipboard"></button>
            <pre${preAttrs}><code${codeAttrs}>`;
      });
  
      // Regex to match </code></pre> closing tags
      wrapped = wrapped.replace(/<\/code><\/pre>/g, "</code></pre></div>");
      return wrapped;
    }
    return content;
  });
  
  


  // Passthrough copies
  eleventyConfig.addPassthroughCopy("public");
  eleventyConfig.addPassthroughCopy({"./src/public/favicon.ico": "favicon.ico"});
  eleventyConfig.addPassthroughCopy({ "./src/public/images/favicon": "/" });
  eleventyConfig.addPassthroughCopy({"./src/robots.txt": "robots.txt"});

  // Additional passthrough for docs assets if needed
  eleventyConfig.addPassthroughCopy("src/assets");

  eleventyConfig.setBrowserSyncConfig({ ghostMode: false });

  return {
    dir: {
      input: "src",
      includes: "_includes",
      output: "build"
    }
  };
};
